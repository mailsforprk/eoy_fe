import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  rForm: FormGroup;
  post:any;
  eName:string = '';
  eNumber:number;
  password:string = '';
  designation:string = '';
  serviceLine:string = '';
  role:string = '';

  constructor(private fb: FormBuilder) {
    this.rForm = fb.group({
      'eName': [null, Validators.required],
      'eNumber': [null, Validators.compose([Validators.required, Validators.max(15)])],
      'password': [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20)])],
      'designation': [null, Validators.required],
      'serviceLine': [null, Validators.required],
      'role': [null, Validators.required],
      'validate' : ''
    });
  }

  addPost(post) {
    
  }

}
